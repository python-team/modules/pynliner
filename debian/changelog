pynliner (0.8.0-5) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

  [ Andrej Shadura ]
  * Prevent crashes when there are styles that don't apply to any element.
  * Move to the team as a primary maintainer, add myself as an uploader
    (Closes: #901291).
  * Wrap and sort.
  * Set Rules-Requires-Root: no.
  * Use dh-sequence-python3.

 -- Andrej Shadura <andrewsh@debian.org>  Thu, 05 Jan 2023 18:47:08 +0100

pynliner (0.8.0-4) unstable; urgency=medium

  * New upload, this time with binary packages

 -- Sandro Tosi <morph@debian.org>  Thu, 16 Jun 2022 10:10:32 -0400

pynliner (0.8.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jun 2022 21:34:15 -0400

pynliner (0.8.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * drop Python 2 support + use pybuild
  * debian/control
    - bump Standards-Version to 4.4.0 (no changes needed)
  * debian/copyright
    - extend packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Tue, 27 Aug 2019 21:53:06 -0400

pynliner (0.8.0-1) unstable; urgency=medium

  * New upstream release

 -- Sandro Tosi <morph@debian.org>  Sun, 22 Jan 2017 16:33:31 -0500

pynliner (0.7.2-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * Initial release (Closes: #791683)
  * debian/control
    - adjust Vcs-Browser to DPMT standards

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Sandro Tosi <morph@debian.org>  Mon, 26 Sep 2016 17:23:39 +0100
